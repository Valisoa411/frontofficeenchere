
import '../assets/bootstrap/css/bootstrap.min.css';
import { Link } from "react-router-dom";
import { Navbar, Nav, Form, Button } from "react-bootstrap";
import { FormGroup, Label, Input } from 'reactstrap';
import { useState } from 'react';
export default function NavBar() {
    const [token, setToken] = useState(localStorage.getItem("token"));
    const deconnection = () => {
        if(token!=null) {
            setToken(null);
            return;
        }
        fetch('https://encherews-production-e3c1.up.railway.app/clients/logout?token='+token)
        .then((data) => data.json())
        .then((res) => {
            if(res.error) throw new Error(res.error);
            localStorage.setItem("token",null);
            setToken(null);
            window.location = '/login';
        })
        .catch((er) => {
            console.error("error : "+er.message);
        });
    }

    return (
        <>
            <Navbar
                style={{ background: "#e2d8bf" }}
                bg="light"
                expand="md"
                className="navigation-clean-search"
            >
                <Navbar.Brand
                    href="#"
                    style={{
                        fontSize: "22px",
                        fontFamily: "'Abril Fatface', cursive",
                        color: "var(--red)"
                    }}
                >
                    Auction Company&nbsp;
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to="/" active style={{ marginLeft: 0 }}>
                            Auction List
                        </Nav.Link>
                        <Nav.Link as={Link} to="/Filter">
                            Filtrer
                        </Nav.Link>
                        <Nav.Link as={Link} to="/Explore">
                            Explore
                        </Nav.Link>

                    </Nav>

                    <Form inline className="form-inline mr-auto" target="_self" method="GET">
                        <FormGroup style={{ marginLeft: '100px' }}>
                            <Label for="search-field">
                                <i className="fa fa-search" style={{ color: 'var(--gray-dark)' }} />
                            </Label>
                            <Input
                                type="search"
                                id="search-field"
                                name="search"
                                className="form-control search-field"
                                style={{ borderStyle: 'solid', borderColor: 'var(--red)', marginLeft: '1px', borderRadius: '22px' }}
                            />
                        </FormGroup>
                        <Button color="light" className="btn btn-light action-button" style={{ marginLeft: '10px', marginTop: '-20px', borderColor: 'var(--red)', background: 'var(--red)' }}>
                            Search
                        </Button>
                    </Form>
                    {!token ? (<a href="/login"><Button>Connection</Button></a>) : (<Button onClick={deconnection}>Deconnection</Button>)}
                </Navbar.Collapse>
            </Navbar>

        </>
    );
}
