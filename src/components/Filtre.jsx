 import React, { useState, useEffect } from "react";
import './Filtre.css';
import Footer1 from './Footer1';
import NavBar from './NavBar';
import EnchereList from "../components/EnchereList";
import Fiche from "../components/Fiche";
export default function Filtre() {
    const [list_, setList] = useState([]);
    const [selectedOption, setSelectedOption] = useState('');
    const [encheres, setEncheres] = useState([]);
    const [selectedItem, setSelectedItem] = useState(null);
    const [openModal, setOpenModal] = useState(false);

    const handleClick = (item) => {
        setSelectedItem(item);
        setOpenModal(true);
    }
    const handleClose = () => setSelectedItem(null);
    

    useEffect(() => {
        fetch("https://encherews-production-e3c1.up.railway.app/categorie")
            .then((data) => data.json())
            .then((res) => {
                console.log(res.data.data);
                setList(res.data.data.listCategorie);
            });
    }, []);

    const[motcle,setMotcle] = useState('');
    const [d1,setD1] = useState('');
    const [d2,setD2] = useState('');
    const [categories,setCategories] = useState([]);
    
    const handleChange = (event) => {
        const value = event.target.value;
        if (event.target.checked) {
          setCategories([...categories, value]);
        } else {
          setCategories(categories.filter((val) => val !== value));
        }
      };
    

    const handleFilter = async(event)=> {
        event.preventDefault();
        console.log("MOt cle"+motcle);
         console.log("Date 1"+d1);
        console.log("Date 2"+d2);
        console.log("Categorie"+categories);
        console.log("option"+selectedOption);

        
        
        var lool = "";
        // const categorie =categories.map(((group)=>(
            
        //         lool+="&categorie"+group;
            
        // ));
        categories.forEach(e => {
            lool+="&categories="+e;
        })
        var url = "https://encherews-production-e3c1.up.railway.app/encheres/filtrer?motcle="+motcle+"&d1="+d1+"&d2="+d2+"&statut="+selectedOption+lool;
        await fetch(url)
        .then((response)=>response.json())
        .then((resultat)=>{
        if(resultat.error){
            //console.log(resultat.error);
            throw new Error(resultat.error.message);
        }
            console.log("result : "+resultat.data);
            setEncheres(resultat.data.data.listenchere);
        })
        .catch((error)=>{
        console.log("error: "+error.message);

        });
    //console.log('ato')
    //response.json()
        //console.log(url);
        
        //const filter = await();
        
    }
    const allCateg = list_.map(group => (
        <div key={group.id}>
          <input type="checkbox" name={group.id} value={group.id} onChange={handleChange}/>
          <label>{group.libelle}</label>
        </div>
    ));

    return (
        <div>
            <NavBar></NavBar>
            
            <div className="container">
                <div className="row articles">
                    <EnchereList encheres={encheres} handleClick={handleClick} />
                    {selectedItem && <Fiche enchere={selectedItem} handleClose={handleClose} isOpen={openModal} />}
                </div>
                <form onSubmit={handleFilter}>

                
                <div className="tittle">Recherche multi</div>
                <div  className="date">
                    <div className="titre">Mot Clé:</div>
                    <input id="datedeb" type="text" name="motcle" onChange={(event)=>setMotcle(event.target.value)}></input>
                    
                </div>
                <div  className="date">
                    <div className="titre">Date:</div>
                    <input id="datedeb" type="datetime-local" name="dateDebut" onChange={(event)=>setD1(event.target.value)}></input>
                    <input id="datefin" type="datetime-local" name="dateFin" onChange={(event)=>setD2(event.target.value)}></input>
                </div>
                <div id="checkbox">
                    <div className="titre">Categorie:</div>
                    {allCateg}
                </div>
                
                <div className="titre" >Choose an option:</div>
                <div>
                    <div className="option">
                        <div className="opt">
                            <input
                                type="radio"
                                value="11"
                                id="ion"
                                name="option"
                                checked={selectedOption === "11"}
                                onChange={() => setSelectedOption("11")} />
                            <label htmlFor="ion">cours</label>
                        </div>
                        <div className="opt">
                            <input
                                type="radio"
                                value="21"
                                id="ion"
                                name="option"
                                checked={selectedOption === "21"}
                                onChange={() => setSelectedOption("21")} />
                            <label htmlFor="ion">Terminer</label>
                        </div>
                    </div>
                </div>
                <button style={{ background: 'lightblue', color: '-moz-initial',padding: '10px 20px',borderRadius: '5px',border: 'none', cursor: 'pointer',}}>Rechercher</button>
            
                </form>
            </div>
            <Footer1></Footer1>
        </div>
    );
}
