const EnchereList = ({ encheres, handleClick }) => (
    <>
        {encheres.map(e => (
            <div key={e.id} className="col-sm-6 col-md-4 item"  onClick={() => handleClick(e)}>
                <h3 className="name">{e.nomProduit}</h3>
                <a>Prix:{e.montant}Ar</a>
                <p className="description">
                {e.description}
                </p>
            </div>
        ))}
    </>
);

export default EnchereList;