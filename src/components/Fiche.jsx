import { useEffect, useState } from 'react';
// import './Fiche.css';
import { Modal, Button, Form } from 'react-bootstrap';
import { FormGroup, Label, Input } from 'reactstrap';


const Fiche = ({ enchere, handleClose, isOpen }) => {
  const [inputValue,setInputValue] = useState(0);
  const [currentEnchere,setCurrentEnchere] = useState(enchere);
  const [token, setToken] = useState(localStorage.getItem("token"));
  const [images, setImages] = useState([]);
  const [open, setOpen] = useState(isOpen);

  useEffect(() => {
    setCurrentEnchere(enchere);
  }, [enchere]);

  useEffect(() => {
    fetch('https://encherews-production-e3c1.up.railway.app/enchereImages/'+currentEnchere.id)
    .then((data) => data.json())
    .then((res) => {
      setImages(res.data.data.images);
    })
  }, [currentEnchere]);

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const submitForm = async (event) => {
    event.preventDefault();
    // handleSubmit(currentEnchere.id,inputValue);
    // setInputValue(0);
    if(token==null){
      window.location="/login";
    } else {
        var link = 'https://encherews-production-e3c1.up.railway.app/clients/rencherir/'+currentEnchere.id+"?montant="+inputValue+"&token="+token;
        await fetch(link,{method: "POST"})
        .then((data) => data.json())
        .then((res) => {
            console.log(res.error);
            if(res.error) throw new Error(res.error.message);
            console.log("message : "+res.data.message);
        })
        .catch((er) => {
            if(er.message === 'Exception : java.lang.Exception: Not Connected') deconnection();
            console.error("error : "+er.message);
        });
        await fetch('https://encherews-production-e3c1.up.railway.app/encheres/v/'+currentEnchere.id)
        .then((data) => data.json())
        .then((res) => {
          if(res.error) throw new Error(res.error.message);
          setCurrentEnchere(res.data.data.enc);
        })
        .catch((er) => {
          if(er.message === 'Exception : java.lang.Exception: Not Connected') deconnection();
          console.error("error : "+er.message);
      });
    }
  };
  const deconnection = () => {
    if(token!=null) {
        setToken(null);
        return;
    }
    fetch('https://encherews-production-e3c1.up.railway.app/clients/logout?token='+token)
    .then((data) => data.json())
    .then((res) => {
        if(res.error) throw new Error(res.error);
        localStorage.setItem("token",null);
        setToken(null);
        window.location = '/login';
    })
    .catch((er) => {
        console.error("error : "+er.message);
    });
  }
  
  return (
    <>
      <Modal show={open} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{currentEnchere.nomProduit}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            {images.map(i => (
              <>
                <img height="200" src={i.image} alt="thumbnail"/>
                <p>{i.image}</p>
              </>
            ))}
            <p>Categorie : {currentEnchere.categorie}</p>
            <p>Description : {currentEnchere.description}</p>
            <p>Prix Actuel : {currentEnchere.montant}Ar</p>
            <p>Client : {currentEnchere.client}</p>
            <p>Fin : {currentEnchere.dateFin}</p>
          </div>
          <Form onSubmit={submitForm}>
            <FormGroup>
              <Input type="number" step="0.01" value={inputValue} onChange={handleInputChange} id="encherir"/>
              <Button type='submit'>Encherir</Button>
            </FormGroup>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default Fiche;
