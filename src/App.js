import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './pages/Home';
import Login from './pages/Login';
import InsertImage from './pages/InsertImage';
import Filtre from './components/Filtre';
import './App.css';

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/img" element={<InsertImage />} />
        <Route path="/Filter" element={<Filtre />} />
        <Route path="/Explore" element={<Home />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
