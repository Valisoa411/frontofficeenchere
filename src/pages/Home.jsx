import { useEffect, useState } from "react";
import EnchereList from "../components/EnchereList";
import Fiche from "../components/Fiche";
import Footer1 from "../components/Footer1";
import NavBar from "../components/NavBar";

export default function Home() {
    const [encheres, setEncheres] = useState([]);
    const [selectedItem, setSelectedItem] = useState(null);
    const [openModal, setOpenModal] = useState(false);

    const handleClick = (item) => {
        setSelectedItem(item);
        setOpenModal(true);
    }
    const handleClose = () => setSelectedItem(null);
    
    useEffect(() => {
        fetch('https://encherews-production-e3c1.up.railway.app/encheres/v')
        .then(data => data.json())
        .then(res => {
            setEncheres(res.data.data.listenchere);
        })
    }, []);

    return (
        <>
            <NavBar/>
            <div className="container">
                <div className="intro">
                    <h2 className="text-center">Listes des articles</h2>
                    <p className="text-center">
                    
                    </p>
                </div>
                <div className="row articles">
                    <EnchereList encheres={encheres} handleClick={handleClick} />
                    {selectedItem && <Fiche enchere={selectedItem} handleClose={handleClose} isOpen={openModal} />}
                </div>
            </div>
            <Footer1/>
        </>
    );
}