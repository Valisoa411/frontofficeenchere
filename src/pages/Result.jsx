import { useEffect, useState } from "react";
import EnchereList from "../components/EnchereList";
import Fiche from "../components/Fiche";
import NavBar from "../components/NavBar";
import Footer1 from "../components/Footer1";

import '../assets/bootstrap/css/bootstrap.min.css';
import '../assets/css/Footer-Clean.css';
import '../assets/fonts/ionicons.min.css';

import '../assets/vendor/aos/aos.css';
import '../assets/vendor/bootstrap/css/bootstrap.min.css';
import '../assets/vendor/bootstrap-icons/bootstrap-icons.css';
import '../assets/vendor/boxicons/css/boxicons.min.css';
import '../assets/vendor/glightbox/css/glightbox.min.css';
import '../assets/vendor/remixicon/remixicon.css';
import '../assets/vendor/swiper/swiper-bundle.min.css';
import '../assets/css/style.css';

export default function Result(props) {
    const [encheres, setEncheres] = useState(props.list);
    const [selectedItem, setSelectedItem] = useState(null);

    const handleClick = (item) => setSelectedItem(item);
    const handleClose = () => setSelectedItem(null);
    
    return (
        <>
            <NavBar/>
            {selectedItem ? (
                <section className="article-list">
                    <div className="container">
                    <div className="intro">
                        <h2 className="text-center">Listes des articles</h2>
                        <p className="text-center">
                        
                        </p>
                    </div>
                    <div className="row articles">
                        <EnchereList encheres={encheres} handleClick={handleClick} />
                        {selectedItem && <Fiche enchere={selectedItem} handleClose={handleClose} />}
                    </div>
                    </div>
                </section>
            ) : (
                <p>No Corresponding result ＞﹏＜</p>
            )}
            <Footer1/>
        </>
    );
}