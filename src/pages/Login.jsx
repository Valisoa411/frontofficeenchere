import { useState } from "react";

import '../assets/bootstrap/css/bootstrap.min.css';
import '../assets/css/Footer-Clean.css';
import '../assets/css/Login-Form-Clean.css';
import '../assets/fonts/ionicons.min.css';

export default function Login() {
    const [error, setError] = useState('');
    const [formData, setFormData] = useState({
        email: 'rasoa@gmail.com',
        mdp: '1234'
    });

    const handleChange = event => {
        setFormData({
          ...formData,
          [event.target.name]: event.target.value
        });
      };

    const submitForm = event => {
        event.preventDefault();
        fetch("https://encherews-production-e3c1.up.railway.app/clients/login?email="+formData.email+"&mdp="+formData.mdp, {method: "POST"})
        .then((data) => data.json())
        .then((res) => {
            if(res.error) {
                throw new Error(res.error.message);
            }
            localStorage.setItem("token", res.data.data.token);
            window.location = '/home';
        })
        .catch((error) => {
            console.log("error : " + error.message);
            setError(error.message);
        });
    }
    

    return (
        <>
            <section className="login-clean">
                <form onSubmit={submitForm}>
                    <h2 className="sr-only">Login Form</h2>
                    <div className="illustration"><i className="icon ion-android-time" style={{color: 'var(--primary)'}}></i></div>
                    <div className="form-group"><input className="form-control" type="text" name="email" value={formData.email} onChange={handleChange}/></div>
                    <div className="form-group"><input className="form-control" type="password" name="mdp" value={formData.mdp} onChange={handleChange}/></div>
                    <div className="form-group"><button className="btn btn-primary btn-block" type="submit" style={{background: 'var(--primary)'}}>Log In</button></div><a className="forgot" href="#">Forgot your email or password?</a>
                </form>
            </section>
        </>
    );
}