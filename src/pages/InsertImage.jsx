import { useState } from "react";

export default function InsertImage(){
    const [data,setData] = useState({
        photo: null,
        id: null
    });
    
    const handleChange = event => {
        setData({
            ...data,
            [event.target.name]: event.target.value
        });
    }

    const upFile = event => {
        event.preventDefault();
        let url = "http://localhost:8080/enchereImages/enchereImage";
        console.log(url);
        fetch(url, {
            method: 'POST',
            headers: {
                "idEnchere": data.id
            },
            body: JSON.stringify({
                image: data.photo
            })
        }).then((result) => {
            console.log("photo: "+data.photo);
            console.log("idEnchere: " + data.id);
            console.log("Result: "+result.json());
            return result.json();

        }).then((e) => {
            console.log(e);
            if (e.error !== undefined) {
                // setExp(true);
            } else {
                // setList(e.data);
                // setOk(true);
            }

        }, (e) => {
        });

        return (
            <>
                <form onSubmit={upFile}>
                    <input type="file" name="photo" onChange={handleChange}/>
                    <input type="text" name="id" onChange={handleChange}/>
                    <input type="submit" value="upload"/>
                </form>
            </>
        );
    }
}